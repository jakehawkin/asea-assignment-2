﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Assignment_2;
using System.Data.SqlServerCe;
using System.Diagnostics;
using System.IO;

namespace Assignment_2
{
    public partial class Form2 : Form
    {        
        public Form2(string s)
        {
            InitializeComponent();
            label2.Text = s;//Sets a label to the current logged in user
            dbCon.Open();
        }

        public SqlCeConnection dbCon = 
            new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");

        public Form2()
        {
            // TODO: Complete member initialization
        }
        public Form RefToForm1 //provides a reference to form 1 so it can be opened from the background
        {
            get;
            set;
        }
        public void Form2_FormClosing(object sender, FormClosingEventArgs e)//returns to log in if the form is closed
        {
            this.RefToForm1.Show();
        }

        public void button1_Click(object sender, EventArgs e)//Returns to log in form
        {
            this.RefToForm1.Show();
            this.Close();
        }

        public void getPizza(int id)
        {
            try
            {
                string[] array = new string[3];//creates a string array
                ListViewItem item;//Creates a new listview item
                SqlCeCommand pizzaPull = dbCon.CreateCommand();//Creates the command
                pizzaPull.CommandText = ("SELECT * FROM pizza WHERE id=@id"); //Selecting ALL from the database using pizza id
                pizzaPull.Parameters.AddWithValue("@id", id);
                SqlCeDataReader mySqlDataReader = pizzaPull.ExecuteReader();
                try
                {
                    mySqlDataReader.Read();//Begin reading 
                    string pizzaName = mySqlDataReader["pizzaName"].ToString();//gets name from database and formats it
                    string pricePull = mySqlDataReader["price"].ToString();//gets price from database and formats
                    float.Parse(pricePull);//parses the price into a float

                    array[0] = pizzaName;//assigns retreived pizza name to array[0]
                    array[1] = pricePull;//assigns price to array[1]
                    item = new ListViewItem(array);//creates the listview using array. I used an array to assign the pizza name as main item and price as sub item
                    listView1.Items.Add(item);//creates the item
                }
                catch (InvalidOperationException)//If it cannot find the pizza in the data base
                {
                    MessageBox.Show("Error", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                }
            }
            catch (SqlCeException ex)//Catch database connection error
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        string[] array = new string[3];//creates a string array
        ListViewItem item;//Creates a new listview item

        public void margButton_Click(object sender, EventArgs e)//activated when the Margerita button is clicked
        {
            getPizza(5);
            clearDiscount();

        }

        public void pepperoniButton_Click(object sender, EventArgs e)//Essentially same as margerita button... see margButton_Click
        {
            getPizza(6);
            clearDiscount();
        }


        public void chickMushButton_Click(object sender, EventArgs e)//Essentially same as margerita button... see margButton_Click
        {
            getPizza(7);
            clearDiscount();
        }

        private void chickPepButton_Click(object sender, EventArgs e)//Essentially same as margerita button... see margButton_Click
        {
            getPizza(8);
            clearDiscount();
        }

        private void bbqChickButton_Click(object sender, EventArgs e)//Essentially same as margerita button... see margButton_Click
        {
            getPizza(9);
            clearDiscount();
        }

        private void vegeButton_Click(object sender, EventArgs e)//Essentially same as margerita button... see margButton_Click
        {
            getPizza(10);
            clearDiscount();
        }

        private void hawaiiButton_Click(object sender, EventArgs e)//Essentially same as margerita button... see margButton_Click
        {
            getPizza(11);
            clearDiscount();
        }

        private void hotSpicyButton_Click(object sender, EventArgs e)//Essentially same as margerita button... see margButton_Click
        {
            getPizza(12);
            clearDiscount();
        }

        private void meatButton_Click(object sender, EventArgs e)//Essentially same as margerita button... see margButton_Click
        {
            getPizza(13);
            clearDiscount();
        }

        public void clearDiscount()
        {
            discountPrice.ResetText();
            discountAmount.ResetText();
        }

        private void listView1_ItemActivate(object sender, EventArgs e)//this controls the deleting of items in the listview
        {
            while (listView1.SelectedItems.Count > 0)
            {
                listView1.Items.Remove(listView1.SelectedItems[0]);//gets selected item and removes it
                clearDiscount();
            }

        }

        public void discount(double discountAmt)
        {
            try
            {
                string totalwithpound = totalPriceLabel.Text.Remove(0, 1);//gets the total price, and removes the pound
                double totalperc = Convert.ToDouble(totalwithpound);//converts the text to double
                double perc = totalperc * discountAmt / 100;//gets the percentage amount
                totalperc = totalperc - perc;//subtracts the percentage amount from the total price
                discountPrice.Text = String.Format("{0:C}", totalperc);//sets the discounted price formatted for currency
                discountAmount.Text = String.Format("{0:C}", perc);//shows how much discount has been applied using currency format
            }
            catch (ArgumentOutOfRangeException)//catches error if no items are in the listview. Cannot apply discount unless items are in the listview
            {
                MessageBox.Show("No Products to Discount", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);

            }
        }
        /*****************Testing Purposes******************/
        /*public double discount(double totalperc, double discountAmt)
        {
            try
            {
                //string totalwithpound = totalPriceLabel.Text.Remove(0, 1);//gets the total price, and removes the pound
                //double totalperc = Convert.ToDouble(totalwithpound);//converts the text to double
                double perc = totalperc * discountAmt / 100;//gets the percentage amount
                totalperc = totalperc - perc;//subtracts the percentage amount from the total price
                //discountPrice.Text = String.Format("{0:C}", totalperc);//sets the discounted price formatted for currency
                //discountAmount.Text = String.Format("{0:C}", perc);//shows how much discount has been applied using currency format
                totalperc = Math.Round(totalperc, 2);
                return totalperc;
            }
            catch (ArgumentOutOfRangeException)//catches error if no items are in the listview. Cannot apply discount unless items are in the listview
            {
                return totalperc;
                MessageBox.Show("No Products to Discount", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);

            }
        }*/

        private void fivepercButton_Click(object sender, EventArgs e)//This applies 5% discount
        {
            discount(5);
        }

        private void tenpercButton_Click(object sender, EventArgs e)//Same as fivepercButton_Click except with 10%
        {
            discount(10);
        }

        private void fifteenpercButton_Click(object sender, EventArgs e)//Same as fivepercButton_Click except with 15%
        {
            discount(15);
        }

        private void seventeenpercButton_Click(object sender, EventArgs e)//Same as fivepercButton_Click except with 17.5%
        {
            discount(17.5);
        }

        private void twentypercButton_Click(object sender, EventArgs e)//Same as fivepercButton_Click except with 20%
        {
            discount(20);
        }

        private void clearItems_Click(object sender, EventArgs e)//erase the listview contents and reset all the labels
        {
            listView1.Items.Clear();
            discountPrice.ResetText();
            discountAmount.ResetText();
            totalPriceLabel.ResetText();
            noItemLabel.ResetText();
        }

        private void Form2_MouseMove(object sender, MouseEventArgs e)//this allows me to constantly update the total prices and no of items. Could not find a better solution
        {
            noItemLabel.Text = listView1.Items.Count.ToString();//set number of items using count
            double total = 0;
            foreach (ListViewItem item in listView1.Items)// for each item add the sub item, this being price
            {
                total += Convert.ToDouble(item.SubItems[1].Text);
            }
            totalPriceLabel.Text = String.Format("{0:C}", total);//set the total price label
        }

        private void printBill(ListView lv)//This prints the listview/bill to file
        {
            using (TextWriter sw = new StreamWriter(@"I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\printedBill.txt"))//creates a stream writer 
            {
                sw.WriteLine("----------------------------\r\n" + " Pizza" + "              " + "Price" + "\r\n____________________________\r\n");//some formatting for the printed bill/reciept
                foreach (ListViewItem item in lv.Items)//for every item print and format using split
                {
                    sw.WriteLine("{0}{1}{2}", item.SubItems[0].Text, " | ", item.SubItems[1].Text);
                }
                sw.WriteLine("____________________________\r\n" +
                "\r\nTill User: " + label2.Text.ToString() +
                "\r\nNumber of Items on Bill: " + noItemLabel.Text.ToString() +
                "\r\nDiscount applied (If any):" + discountAmount.Text.ToString() +
                "\r\nTotal Price: " + totalPriceLabel.Text.ToString() +
                "\r\nTotal Price with Discount: " + discountPrice.Text.ToString() +
                "\r\n----------------------------\r\n", true);//further formatting adding prices and items
                sw.Close();
            }    
        }

        private void printBillButton_Click(object sender, EventArgs e)//this prints the bill. It passes the data to the printBill method and then opens the text file, simulating a printed bill, sort of. 
        {
            if(listView1.Items.Count > 0)
            {
                printBill(listView1);
                Process.Start(@"I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\printedBill.txt");
            }
            else
            {
                MessageBox.Show("Nothing to Print!", "Try Again",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);
            }
        }

            
        
    }
}

