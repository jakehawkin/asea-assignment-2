﻿namespace Assignment_2_test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.username_Entry = new System.Windows.Forms.TextBox();
            this.password_Entry = new System.Windows.Forms.TextBox();
            this.log_In_Button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.reset_Button = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.register_Button = new System.Windows.Forms.Button();
            this.database2DataSet1 = new Assignment_2.Database2DataSet1();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.database2DataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // username_Entry
            // 
            this.username_Entry.Location = new System.Drawing.Point(389, 237);
            this.username_Entry.Name = "username_Entry";
            this.username_Entry.Size = new System.Drawing.Size(194, 20);
            this.username_Entry.TabIndex = 1;
            this.username_Entry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.username_Entry_KeyDown);
            // 
            // password_Entry
            // 
            this.password_Entry.BackColor = System.Drawing.SystemColors.Window;
            this.password_Entry.Location = new System.Drawing.Point(389, 272);
            this.password_Entry.Name = "password_Entry";
            this.password_Entry.PasswordChar = '*';
            this.password_Entry.Size = new System.Drawing.Size(194, 20);
            this.password_Entry.TabIndex = 2;
            this.password_Entry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.password_Entry_KeyDown);
            // 
            // log_In_Button
            // 
            this.log_In_Button.Location = new System.Drawing.Point(388, 298);
            this.log_In_Button.Name = "log_In_Button";
            this.log_In_Button.Size = new System.Drawing.Size(98, 32);
            this.log_In_Button.TabIndex = 3;
            this.log_In_Button.Text = "Log In";
            this.log_In_Button.UseVisualStyleBackColor = true;
            this.log_In_Button.Click += new System.EventHandler(this.log_In_Button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(385, 210);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Login";
            // 
            // reset_Button
            // 
            this.reset_Button.Location = new System.Drawing.Point(437, 338);
            this.reset_Button.Name = "reset_Button";
            this.reset_Button.Size = new System.Drawing.Size(96, 32);
            this.reset_Button.TabIndex = 6;
            this.reset_Button.Text = "Reset";
            this.reset_Button.UseVisualStyleBackColor = true;
            this.reset_Button.Click += new System.EventHandler(this.reset_Button_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(176, 197);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(203, 138);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // register_Button
            // 
            this.register_Button.Location = new System.Drawing.Point(487, 298);
            this.register_Button.Name = "register_Button";
            this.register_Button.Size = new System.Drawing.Size(96, 32);
            this.register_Button.TabIndex = 8;
            this.register_Button.Text = "Register";
            this.register_Button.UseVisualStyleBackColor = true;
            this.register_Button.Click += new System.EventHandler(this.register_Button_Click);
            // 
            // database2DataSet1
            // 
            this.database2DataSet1.DataSetName = "Database2DataSet1";
            this.database2DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(678, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 32);
            this.button1.TabIndex = 9;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.register_Button);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.reset_Button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.log_In_Button);
            this.Controls.Add(this.password_Entry);
            this.Controls.Add(this.username_Entry);
            this.Name = "Form1";
            this.Text = "Log-In";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.database2DataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox username_Entry;
        private System.Windows.Forms.TextBox password_Entry;
        private System.Windows.Forms.Button log_In_Button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button reset_Button;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button register_Button;
        private Assignment_2.Database2DataSet1 database2DataSet1;
        private System.Windows.Forms.Button button1;
    }
}

