﻿using System;
using System.Windows.Forms;
using Assignment_2;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Assignment_2_Unit_Testing
{
    [TestClass]
    public class EncryptTest
    {
        [TestMethod]
        public void encryptTest()
        {
            String input = "password";
            String output;
            output = Form3.Encrypt(input);

            MessageBox.Show(output, "Output",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Question);
        }
    }
}
