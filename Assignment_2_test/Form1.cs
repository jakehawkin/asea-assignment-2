﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Assignment_2;
using System.Data.SqlServerCe;
using System.Security.Cryptography;

namespace Assignment_2_test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            username_Entry.Select();//sets focus on username input
        }
        private void log_In_Button_Click(object sender, EventArgs e)
        {
            String username = username_Entry.Text; //Get Username
            String password = password_Entry.Text; //Get Password
            
            if (username == "" && password == "")//Checking to see if blank data is submitted
            {
                MessageBox.Show("Blank data submitted!", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                username_Entry.Select();
            }
            else
            {
                SqlCeConnection dbCon =
                        new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");//Points to database
                try
                {
                    dbCon.Open();//opens database connection

                    SqlCeCommand mySqlCommandCheck = dbCon.CreateCommand();//Creating username check command
                    mySqlCommandCheck.CommandText = ("SELECT * FROM users WHERE username=@username"); //Selecting ALL from the database
                    mySqlCommandCheck.Parameters.AddWithValue("@username", username);

                    SqlCeDataReader mySqlDataReader = mySqlCommandCheck.ExecuteReader();//Begins reading the database

                    try//tries the if statement to mathc a username and password. If it fails it throws an exception which is caught by the catch
                    {
                        mySqlDataReader.Read();
                        if (mySqlDataReader["username"].ToString() == username_Entry.Text && mySqlDataReader["password"].ToString() == password_Entry.Text)//matches username and password inputs to the database
                        {
                            Form2 orderForm = new Form2();//Creates form 2
                            orderForm.RefToForm1 = this;//Creates reference for form 2 and 3 to re-open form 1
                            this.Hide();
                            orderForm.Show();
                            username_Entry.Clear();
                            password_Entry.Clear();
                        }
                    }
                    catch (InvalidOperationException)//catches the exception. The exception only occurs when there is no match, so I can use it to tell the user it is an invalid username or password
                    {
                        MessageBox.Show("Invalid Username or Password!", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                    }
                    dbCon.Close();
                }
                catch (SqlCeException ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);//throws error if connection cannot be opened and gives a reason why
                }  
            } 
        }
        private void register_Button_Click(object sender, EventArgs e)//opens register form 
        {
            Form3 registerForm = new Form3();
            registerForm.RefToForm1 = this;
            this.Hide();
            registerForm.Show();
            username_Entry.Clear();
            password_Entry.Clear();
        }
        private void reset_Button_Click(object sender, EventArgs e)//resets username and password entry fields and focuses on username
        {
            username_Entry.Clear();
            password_Entry.Clear();
            username_Entry.Select();
        }

        private void username_Entry_KeyDown(object sender, KeyEventArgs e)//allows user to use the enter key to move through input fields
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void password_Entry_KeyDown(object sender, KeyEventArgs e)//allows user to use the enter key to move through input fields
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void button1_Click(object sender, EventArgs e)//exit button
        {
            this.Close();
        }  
    }
}
