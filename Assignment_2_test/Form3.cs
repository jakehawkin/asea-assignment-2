﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace Assignment_2
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
        public Form RefToForm1//Provides a reference to form 1 so that it can be reopened
        {
            get;
            set;
        }
        private void Form3_FormClosing(object sender, FormClosingEventArgs e)//Returns to log in if form is closed
        {
            this.RefToForm1.Show();
        }

        private void button1_Click(object sender, EventArgs e)//Returns to log in
        {
            this.RefToForm1.Show();
            this.Close();
        }

    }
}
