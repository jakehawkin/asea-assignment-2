﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Assignment_2;
using System.Data.SqlServerCe;

namespace Assignment_2
{
    public partial class Form2 : Form
    {
        public Form2(string s)
        {
            InitializeComponent();
            label2.Text = s;

        }
        public Form RefToForm1 //provides a reference to form 1 so it can be opened from the background
        { 
            get; 
            set; 
        }
        private void Form2_FormClosing(object sender, FormClosingEventArgs e)//returns to log in if the form is closed
        {
            this.RefToForm1.Show();
        }

        private void button1_Click(object sender, EventArgs e)//Returns to log in form
        {
            this.RefToForm1.Show();
            this.Close();

        }

        string[] array = new string[3];
        ListViewItem item;

        private void margButton_Click(object sender, EventArgs e)
        {
            SqlCeConnection dbCon = new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");
            try
            {
                dbCon.Open();
                SqlCeCommand pizzaPull = dbCon.CreateCommand();//Creating username check command
                pizzaPull.CommandText = ("SELECT * FROM pizza WHERE id='5'"); //Selecting ALL from the database
                SqlCeDataReader mySqlDataReader = pizzaPull.ExecuteReader();
                try
                {
                    mySqlDataReader.Read();
                    string pizzaName = mySqlDataReader["pizzaName"].ToString();
                    string pricePull = mySqlDataReader["price"].ToString();
                    float.Parse(pricePull);

                    array[0] = pizzaName;
                    array[1] = pricePull;
                    item = new ListViewItem(array);
                    listView1.Items.Add(item);
                    listView1.Select();
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Error", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pepperoniButton_Click(object sender, EventArgs e)
        {
            SqlCeConnection dbCon = new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");
            try
            {
                dbCon.Open();
                SqlCeCommand pizzaPull = dbCon.CreateCommand();//Creating username check command
                pizzaPull.CommandText = ("SELECT * FROM pizza WHERE id='6'"); //Selecting ALL from the database
                SqlCeDataReader mySqlDataReader = pizzaPull.ExecuteReader();
                try
                {
                    mySqlDataReader.Read();
                    string pizzaName = mySqlDataReader["pizzaName"].ToString();
                    string pricePull = mySqlDataReader["price"].ToString();
                    float.Parse(pricePull);

                    array[0] = pizzaName;
                    array[1] = pricePull;
                    item = new ListViewItem(array);
                    listView1.Items.Add(item);
                    listView1.Select();
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Error", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chickMushButton_Click(object sender, EventArgs e)
        {
            SqlCeConnection dbCon = new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");
            try
            {
                dbCon.Open();
                SqlCeCommand pizzaPull = dbCon.CreateCommand();//Creating username check command
                pizzaPull.CommandText = ("SELECT * FROM pizza WHERE id='7'"); //Selecting ALL from the database
                SqlCeDataReader mySqlDataReader = pizzaPull.ExecuteReader();
                try
                {
                    mySqlDataReader.Read();
                    string pizzaName = mySqlDataReader["pizzaName"].ToString();
                    string pricePull = mySqlDataReader["price"].ToString();
                    float.Parse(pricePull);

                    array[0] = pizzaName;
                    array[1] = pricePull;
                    item = new ListViewItem(array);
                    listView1.Items.Add(item);
                    listView1.Select();

                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Error", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chickPepButton_Click(object sender, EventArgs e)
        {
            SqlCeConnection dbCon = new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");
            try
            {
                dbCon.Open();
                SqlCeCommand pizzaPull = dbCon.CreateCommand();//Creating username check command
                pizzaPull.CommandText = ("SELECT * FROM pizza WHERE id='8'"); //Selecting ALL from the database
                SqlCeDataReader mySqlDataReader = pizzaPull.ExecuteReader();
                try
                {
                    mySqlDataReader.Read();
                    string pizzaName = mySqlDataReader["pizzaName"].ToString();
                    string pricePull = mySqlDataReader["price"].ToString();
                    float.Parse(pricePull);

                    array[0] = pizzaName;
                    array[1] = pricePull;
                    item = new ListViewItem(array);
                    listView1.Items.Add(item);
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Error", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bbqChickButton_Click(object sender, EventArgs e)
        {
            SqlCeConnection dbCon = new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");
            try
            {
                dbCon.Open();
                SqlCeCommand pizzaPull = dbCon.CreateCommand();//Creating username check command
                pizzaPull.CommandText = ("SELECT * FROM pizza WHERE id='9'"); //Selecting ALL from the database
                SqlCeDataReader mySqlDataReader = pizzaPull.ExecuteReader();
                try
                {
                    mySqlDataReader.Read();
                    string pizzaName = mySqlDataReader["pizzaName"].ToString();
                    string pricePull = mySqlDataReader["price"].ToString();
                    float.Parse(pricePull);

                    array[0] = pizzaName;
                    array[1] = pricePull;
                    item = new ListViewItem(array);
                    listView1.Items.Add(item);
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Error", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void vegeButton_Click(object sender, EventArgs e)
        {
            SqlCeConnection dbCon = new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");
            try
            {
                dbCon.Open();
                SqlCeCommand pizzaPull = dbCon.CreateCommand();//Creating username check command
                pizzaPull.CommandText = ("SELECT * FROM pizza WHERE id='10'"); //Selecting ALL from the database
                SqlCeDataReader mySqlDataReader = pizzaPull.ExecuteReader();
                try
                {
                    mySqlDataReader.Read();
                    string pizzaName = mySqlDataReader["pizzaName"].ToString();
                    string pricePull = mySqlDataReader["price"].ToString();
                    float.Parse(pricePull);

                    array[0] = pizzaName;
                    array[1] = pricePull;
                    item = new ListViewItem(array);
                    listView1.Items.Add(item);
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Error", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void hawaiiButton_Click(object sender, EventArgs e)
        {
            SqlCeConnection dbCon = new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");
            try
            {
                dbCon.Open();
                SqlCeCommand pizzaPull = dbCon.CreateCommand();//Creating username check command
                pizzaPull.CommandText = ("SELECT * FROM pizza WHERE id='11'"); //Selecting ALL from the database
                SqlCeDataReader mySqlDataReader = pizzaPull.ExecuteReader();
                try
                {
                    mySqlDataReader.Read();
                    string pizzaName = mySqlDataReader["pizzaName"].ToString();
                    string pricePull = mySqlDataReader["price"].ToString();
                    float.Parse(pricePull);

                    array[0] = pizzaName;
                    array[1] = pricePull;
                    item = new ListViewItem(array);
                    listView1.Items.Add(item);
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Error", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void hotSpicyButton_Click(object sender, EventArgs e)
        {
            SqlCeConnection dbCon = new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");
            try
            {
                dbCon.Open();
                SqlCeCommand pizzaPull = dbCon.CreateCommand();//Creating username check command
                pizzaPull.CommandText = ("SELECT * FROM pizza WHERE id='12'"); //Selecting ALL from the database
                SqlCeDataReader mySqlDataReader = pizzaPull.ExecuteReader();
                try
                {
                    mySqlDataReader.Read();
                    string pizzaName = mySqlDataReader["pizzaName"].ToString();
                    string pricePull = mySqlDataReader["price"].ToString();
                    float.Parse(pricePull);

                    array[0] = pizzaName;
                    array[1] = pricePull;
                    item = new ListViewItem(array);
                    listView1.Items.Add(item);
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Error", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void meatButton_Click(object sender, EventArgs e)
        {
            SqlCeConnection dbCon = new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");
            try
            {
                dbCon.Open();
                SqlCeCommand pizzaPull = dbCon.CreateCommand();//Creating username check command
                pizzaPull.CommandText = ("SELECT * FROM pizza WHERE id='13'"); //Selecting ALL from the database
                SqlCeDataReader mySqlDataReader = pizzaPull.ExecuteReader();
                try
                {
                    mySqlDataReader.Read();
                    string pizzaName = mySqlDataReader["pizzaName"].ToString();
                    string pricePull = mySqlDataReader["price"].ToString();
                    float.Parse(pricePull);

                    array[0] = pizzaName;
                    array[1] = pricePull;
                    item = new ListViewItem(array);
                    listView1.Items.Add(item);
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Error", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            while (listView1.SelectedItems.Count > 0)
            {
                listView1.Items.Remove(listView1.SelectedItems[0]);
            }

        }

        private void fivepercButton_Click(object sender, EventArgs e)
        {
            try
            {
                string totalwithpound = totalPriceLabel.Text.Remove(0, 1);
                double totalperc = Convert.ToDouble(totalwithpound);
                double perc = totalperc * 5 / 100;
                totalperc = totalperc - perc;
                discountPrice.Text = "£" + Math.Round(totalperc, 2).ToString();
                discountAmount.Text = "£" + Math.Round(perc, 2).ToString();
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("No Products to Discount", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
            }
        }

        private void tenpercButton_Click(object sender, EventArgs e)
        {
            try
            {
                string totalwithpound = totalPriceLabel.Text.Remove(0, 1);
                double totalperc = Convert.ToDouble(totalwithpound);
                double perc = totalperc * 10 / 100;
                totalperc = totalperc - perc;
                discountPrice.Text = "£" + Math.Round(totalperc, 2).ToString();
                discountAmount.Text = "£" + Math.Round(perc, 2).ToString();
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("No Products to Discount", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
            }
        }

        private void fifteenpercButton_Click(object sender, EventArgs e)
        {
            try
            {
                string totalwithpound = totalPriceLabel.Text.Remove(0, 1);
                double totalperc = Convert.ToDouble(totalwithpound);
                double perc = totalperc * 15 / 100;
                totalperc = totalperc - perc;
                discountPrice.Text = "£" + Math.Round(totalperc, 2).ToString();
                discountAmount.Text = "£" + Math.Round(perc, 2).ToString();
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("No Products to Discount", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
            }
        }

        private void seventeenpercButton_Click(object sender, EventArgs e)
        {
            try
            {
                string totalwithpound = totalPriceLabel.Text.Remove(0, 1);
                double totalperc = Convert.ToDouble(totalwithpound);
                double perc = totalperc * 17.5 / 100;
                totalperc = totalperc - perc;
                discountPrice.Text = "£" + Math.Round(totalperc, 2).ToString();
                discountAmount.Text = "£" + Math.Round(perc, 2).ToString();
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("No Products to Discount", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
            }
        }
        
        private void twentypercButton_Click(object sender, EventArgs e)
        {
            try
            {
                string totalwithpound = totalPriceLabel.Text.Remove(0, 1);
                double totalperc = Convert.ToDouble(totalwithpound);
                double perc = totalperc * 20 / 100;
                totalperc = totalperc - perc;
                discountPrice.Text = "£" + Math.Round(totalperc, 2).ToString();
                discountAmount.Text = "£" + Math.Round(perc, 2).ToString();

            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("No Products to Discount", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
            }
        }

        private void clearItems_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            discountPrice.ResetText();
            discountAmount.ResetText();
        }

        private void Form2_MouseMove(object sender, MouseEventArgs e)
        {
            noItemLabel.Text = listView1.Items.Count.ToString();
            double total = 0;
            foreach (ListViewItem item in listView1.Items)
            {
                total += Convert.ToDouble(item.SubItems[1].Text);
            }
            totalPriceLabel.Text = "£" + Math.Round(total, 2).ToString();
        }

            
        
    }
}

