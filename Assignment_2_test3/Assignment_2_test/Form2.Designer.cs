﻿namespace Assignment_2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.margButton = new System.Windows.Forms.Button();
            this.pepperoniButton = new System.Windows.Forms.Button();
            this.chickMushButton = new System.Windows.Forms.Button();
            this.chickPepButton = new System.Windows.Forms.Button();
            this.bbqChickButton = new System.Windows.Forms.Button();
            this.vegeButton = new System.Windows.Forms.Button();
            this.hawaiiButton = new System.Windows.Forms.Button();
            this.hotSpicyButton = new System.Windows.Forms.Button();
            this.meatButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.totalPriceLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.noItemLabel = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Pizza = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Price = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fivepercButton = new System.Windows.Forms.Button();
            this.tenpercButton = new System.Windows.Forms.Button();
            this.fifteenpercButton = new System.Windows.Forms.Button();
            this.seventeenpercButton = new System.Windows.Forms.Button();
            this.twentypercButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.discountAmount = new System.Windows.Forms.Label();
            this.clearItems = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.discountPrice = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(203, 138);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(683, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 34);
            this.button1.TabIndex = 9;
            this.button1.Text = "Log Out";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(221, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "Currently Logged In As:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(391, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 20);
            this.label2.TabIndex = 11;
            // 
            // margButton
            // 
            this.margButton.Location = new System.Drawing.Point(12, 155);
            this.margButton.Name = "margButton";
            this.margButton.Size = new System.Drawing.Size(139, 102);
            this.margButton.TabIndex = 12;
            this.margButton.Text = "Margerita";
            this.margButton.UseVisualStyleBackColor = true;
            this.margButton.Click += new System.EventHandler(this.margButton_Click);
            // 
            // pepperoniButton
            // 
            this.pepperoniButton.Location = new System.Drawing.Point(157, 155);
            this.pepperoniButton.Name = "pepperoniButton";
            this.pepperoniButton.Size = new System.Drawing.Size(139, 102);
            this.pepperoniButton.TabIndex = 13;
            this.pepperoniButton.Text = "Pepperoni";
            this.pepperoniButton.UseVisualStyleBackColor = true;
            this.pepperoniButton.Click += new System.EventHandler(this.pepperoniButton_Click);
            // 
            // chickMushButton
            // 
            this.chickMushButton.Location = new System.Drawing.Point(302, 155);
            this.chickMushButton.Name = "chickMushButton";
            this.chickMushButton.Size = new System.Drawing.Size(139, 102);
            this.chickMushButton.TabIndex = 14;
            this.chickMushButton.Text = "Chicken + Mushroom";
            this.chickMushButton.UseVisualStyleBackColor = true;
            this.chickMushButton.Click += new System.EventHandler(this.chickMushButton_Click);
            // 
            // chickPepButton
            // 
            this.chickPepButton.Location = new System.Drawing.Point(12, 263);
            this.chickPepButton.Name = "chickPepButton";
            this.chickPepButton.Size = new System.Drawing.Size(139, 102);
            this.chickPepButton.TabIndex = 15;
            this.chickPepButton.Text = "Chicken + Peppers";
            this.chickPepButton.UseVisualStyleBackColor = true;
            this.chickPepButton.Click += new System.EventHandler(this.chickPepButton_Click);
            // 
            // bbqChickButton
            // 
            this.bbqChickButton.Location = new System.Drawing.Point(157, 263);
            this.bbqChickButton.Name = "bbqChickButton";
            this.bbqChickButton.Size = new System.Drawing.Size(139, 102);
            this.bbqChickButton.TabIndex = 16;
            this.bbqChickButton.Text = "BBQ Chicken";
            this.bbqChickButton.UseVisualStyleBackColor = true;
            this.bbqChickButton.Click += new System.EventHandler(this.bbqChickButton_Click);
            // 
            // vegeButton
            // 
            this.vegeButton.Location = new System.Drawing.Point(302, 263);
            this.vegeButton.Name = "vegeButton";
            this.vegeButton.Size = new System.Drawing.Size(139, 102);
            this.vegeButton.TabIndex = 17;
            this.vegeButton.Text = "Vegetarian";
            this.vegeButton.UseVisualStyleBackColor = true;
            this.vegeButton.Click += new System.EventHandler(this.vegeButton_Click);
            // 
            // hawaiiButton
            // 
            this.hawaiiButton.Location = new System.Drawing.Point(12, 371);
            this.hawaiiButton.Name = "hawaiiButton";
            this.hawaiiButton.Size = new System.Drawing.Size(139, 102);
            this.hawaiiButton.TabIndex = 18;
            this.hawaiiButton.Text = "Hawaiian";
            this.hawaiiButton.UseVisualStyleBackColor = true;
            this.hawaiiButton.Click += new System.EventHandler(this.hawaiiButton_Click);
            // 
            // hotSpicyButton
            // 
            this.hotSpicyButton.Location = new System.Drawing.Point(157, 371);
            this.hotSpicyButton.Name = "hotSpicyButton";
            this.hotSpicyButton.Size = new System.Drawing.Size(139, 102);
            this.hotSpicyButton.TabIndex = 19;
            this.hotSpicyButton.Text = "Hot + Spicy";
            this.hotSpicyButton.UseVisualStyleBackColor = true;
            this.hotSpicyButton.Click += new System.EventHandler(this.hotSpicyButton_Click);
            // 
            // meatButton
            // 
            this.meatButton.Location = new System.Drawing.Point(302, 371);
            this.meatButton.Name = "meatButton";
            this.meatButton.Size = new System.Drawing.Size(139, 102);
            this.meatButton.TabIndex = 20;
            this.meatButton.Text = "Meat Feast";
            this.meatButton.UseVisualStyleBackColor = true;
            this.meatButton.Click += new System.EventHandler(this.meatButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(636, 509);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 22;
            this.label3.Text = "Total Price:";
            // 
            // totalPriceLabel
            // 
            this.totalPriceLabel.AutoSize = true;
            this.totalPriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalPriceLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.totalPriceLabel.Location = new System.Drawing.Point(716, 508);
            this.totalPriceLabel.Name = "totalPriceLabel";
            this.totalPriceLabel.Size = new System.Drawing.Size(0, 24);
            this.totalPriceLabel.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(514, 509);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 20);
            this.label5.TabIndex = 24;
            this.label5.Text = "No. of Items: ";
            // 
            // noItemLabel
            // 
            this.noItemLabel.AutoSize = true;
            this.noItemLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noItemLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.noItemLabel.Location = new System.Drawing.Point(608, 507);
            this.noItemLabel.Name = "noItemLabel";
            this.noItemLabel.Size = new System.Drawing.Size(0, 24);
            this.noItemLabel.TabIndex = 25;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Pizza,
            this.Price});
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(518, 53);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(254, 450);
            this.listView1.TabIndex = 26;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            // 
            // Pizza
            // 
            this.Pizza.Text = "Pizza";
            this.Pizza.Width = 190;
            // 
            // Price
            // 
            this.Price.Text = "Price";
            this.Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Price.Width = 59;
            // 
            // fivepercButton
            // 
            this.fivepercButton.Location = new System.Drawing.Point(26, 506);
            this.fivepercButton.Name = "fivepercButton";
            this.fivepercButton.Size = new System.Drawing.Size(75, 23);
            this.fivepercButton.TabIndex = 27;
            this.fivepercButton.Text = "5%";
            this.fivepercButton.UseVisualStyleBackColor = true;
            this.fivepercButton.Click += new System.EventHandler(this.fivepercButton_Click);
            // 
            // tenpercButton
            // 
            this.tenpercButton.Location = new System.Drawing.Point(107, 506);
            this.tenpercButton.Name = "tenpercButton";
            this.tenpercButton.Size = new System.Drawing.Size(75, 23);
            this.tenpercButton.TabIndex = 28;
            this.tenpercButton.Text = "10%";
            this.tenpercButton.UseVisualStyleBackColor = true;
            this.tenpercButton.Click += new System.EventHandler(this.tenpercButton_Click);
            // 
            // fifteenpercButton
            // 
            this.fifteenpercButton.Location = new System.Drawing.Point(188, 506);
            this.fifteenpercButton.Name = "fifteenpercButton";
            this.fifteenpercButton.Size = new System.Drawing.Size(75, 23);
            this.fifteenpercButton.TabIndex = 29;
            this.fifteenpercButton.Text = "15%";
            this.fifteenpercButton.UseVisualStyleBackColor = true;
            this.fifteenpercButton.Click += new System.EventHandler(this.fifteenpercButton_Click);
            // 
            // seventeenpercButton
            // 
            this.seventeenpercButton.Location = new System.Drawing.Point(269, 506);
            this.seventeenpercButton.Name = "seventeenpercButton";
            this.seventeenpercButton.Size = new System.Drawing.Size(75, 23);
            this.seventeenpercButton.TabIndex = 30;
            this.seventeenpercButton.Text = "17.5%";
            this.seventeenpercButton.UseVisualStyleBackColor = true;
            this.seventeenpercButton.Click += new System.EventHandler(this.seventeenpercButton_Click);
            // 
            // twentypercButton
            // 
            this.twentypercButton.Location = new System.Drawing.Point(350, 506);
            this.twentypercButton.Name = "twentypercButton";
            this.twentypercButton.Size = new System.Drawing.Size(75, 23);
            this.twentypercButton.TabIndex = 31;
            this.twentypercButton.Text = "20%";
            this.twentypercButton.UseVisualStyleBackColor = true;
            this.twentypercButton.Click += new System.EventHandler(this.twentypercButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(13, 479);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 24);
            this.label4.TabIndex = 32;
            this.label4.Text = "Discount:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(13, 533);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(158, 20);
            this.label6.TabIndex = 33;
            this.label6.Text = "Amount Discounted: ";
            // 
            // discountAmount
            // 
            this.discountAmount.AutoSize = true;
            this.discountAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discountAmount.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.discountAmount.Location = new System.Drawing.Point(164, 531);
            this.discountAmount.Name = "discountAmount";
            this.discountAmount.Size = new System.Drawing.Size(0, 24);
            this.discountAmount.TabIndex = 34;
            // 
            // clearItems
            // 
            this.clearItems.Location = new System.Drawing.Point(588, 13);
            this.clearItems.Name = "clearItems";
            this.clearItems.Size = new System.Drawing.Size(89, 34);
            this.clearItems.TabIndex = 35;
            this.clearItems.Text = "Clear Items";
            this.clearItems.UseVisualStyleBackColor = true;
            this.clearItems.Click += new System.EventHandler(this.clearItems_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(514, 531);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(186, 20);
            this.label7.TabIndex = 36;
            this.label7.Text = "Total Price with Discount:";
            // 
            // discountPrice
            // 
            this.discountPrice.AutoSize = true;
            this.discountPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discountPrice.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.discountPrice.Location = new System.Drawing.Point(698, 531);
            this.discountPrice.Name = "discountPrice";
            this.discountPrice.Size = new System.Drawing.Size(0, 24);
            this.discountPrice.TabIndex = 37;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.discountPrice);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.clearItems);
            this.Controls.Add(this.discountAmount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.twentypercButton);
            this.Controls.Add(this.seventeenpercButton);
            this.Controls.Add(this.fifteenpercButton);
            this.Controls.Add(this.tenpercButton);
            this.Controls.Add(this.fivepercButton);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.noItemLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.totalPriceLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.meatButton);
            this.Controls.Add(this.hotSpicyButton);
            this.Controls.Add(this.hawaiiButton);
            this.Controls.Add(this.vegeButton);
            this.Controls.Add(this.bbqChickButton);
            this.Controls.Add(this.chickPepButton);
            this.Controls.Add(this.chickMushButton);
            this.Controls.Add(this.pepperoniButton);
            this.Controls.Add(this.margButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form2";
            this.Text = "TillView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form2_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button margButton;
        private System.Windows.Forms.Button pepperoniButton;
        private System.Windows.Forms.Button chickMushButton;
        private System.Windows.Forms.Button chickPepButton;
        private System.Windows.Forms.Button bbqChickButton;
        private System.Windows.Forms.Button vegeButton;
        private System.Windows.Forms.Button hawaiiButton;
        private System.Windows.Forms.Button hotSpicyButton;
        private System.Windows.Forms.Button meatButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label totalPriceLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label noItemLabel;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Pizza;
        private System.Windows.Forms.ColumnHeader Price;
        private System.Windows.Forms.Button fivepercButton;
        private System.Windows.Forms.Button tenpercButton;
        private System.Windows.Forms.Button fifteenpercButton;
        private System.Windows.Forms.Button seventeenpercButton;
        private System.Windows.Forms.Button twentypercButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label discountAmount;
        private System.Windows.Forms.Button clearItems;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label discountPrice;
    }
}