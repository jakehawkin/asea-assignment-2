﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Security.Cryptography;
using System.IO;

namespace Assignment_2
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            username_Entry.Select();
        }
        public Form RefToForm1//Provides a reference to form 1 so that it can be reopened
        {
            get;
            set;
        }
        private void Form3_FormClosing(object sender, FormClosingEventArgs e)//Returns to log in if form is closed
        {
            this.RefToForm1.Show();
        }

        private void button1_Click(object sender, EventArgs e)//Returns to log in
        {
            this.RefToForm1.Show();
            this.Close();
        }

        public static string Encrypt(string input)//Encrypt method for password
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);//takes password and encodes to a byte array
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();//creates the algorithm
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes("keykeykeykey1234");//sets key using a 128 bit key
            tripleDES.Mode = CipherMode.ECB;//Encrypts using ECB
            tripleDES.Padding = PaddingMode.PKCS7;//how it pads the code to reach correct amount of bytes
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();//creates encryptor with myKey
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);//creates hash value of byte array
            tripleDES.Clear();//clears all resources
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);//returns encrypted password
        }

        private void create_Account_Click(object sender, EventArgs e)
        {
            String username = username_Entry.Text; //Get Username
            String encryptPassword = Encrypt(password_Entry.Text.ToString());
            String encryptConfirm = Encrypt(confirm_Entry.Text.ToString());

            if (username == "" && password_Entry.Text == "" && confirm_Entry.Text == "")//Checking to see if blank data is submitted
            {
                MessageBox.Show("Blank data submitted!", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                username_Entry.Select();
            }
            else if (username == "")//Checking to see if blank data is submitted
            {
                MessageBox.Show("Missing Username", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                username_Entry.Select();
            }
            else if (password_Entry.Text == "")//Checking to see if blank data is submitted
            {
                MessageBox.Show("Missing Password", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                password_Entry.Select();
            }
            else if (confirm_Entry.Text == "")//Checking to see if blank data is submitted
            {
                MessageBox.Show("Password Confirm Blank", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                confirm_Entry.Select();
            }
            else
            {
                if (encryptPassword != encryptConfirm)//checking if password and confirm password are the same
                {
                    MessageBox.Show("Confirm Password does not match!", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                    confirm_Entry.Select();
                }
                else
                {
                    SqlCeConnection dbCon =
                        new SqlCeConnection(@"Data Source=I:\ASEA Assignment 2\ASEA Assignment 2\Assignment_2_test\Assignment_2_test\Database2.sdf");//Points to database
                    try
                    {
                        dbCon.Open();//opens database connection

                        SqlCeCommand mySqlCommandCheck = dbCon.CreateCommand();//Creating username check command
                        mySqlCommandCheck.CommandText = ("SELECT * FROM users WHERE username=@username"); //Selecting ALL from the database
                        mySqlCommandCheck.Parameters.AddWithValue("@username", username);

                        SqlCeDataReader mySqlDataReader = mySqlCommandCheck.ExecuteReader();//Begins reading the database

                        try
                        {
                            mySqlDataReader.Read();
                            if (mySqlDataReader["username"].ToString() == username)//checks if username already exists
                            {
                                MessageBox.Show("Username Taken!", "Try Again",
                                 MessageBoxButtons.RetryCancel,
                                 MessageBoxIcon.Error);
                                username_Entry.Select();
                            }
                        }
                        catch (InvalidOperationException)
                        {
                            try
                            {
                                String commandText = "INSERT INTO users(username, password) VALUES (@username, @encryptPassword)"; //Selecting ALL from the database
                                SqlCeCommand registerInsert = new SqlCeCommand(commandText, dbCon);//creates command
                                registerInsert.Parameters.AddWithValue("@username", username);//creates data for string
                                registerInsert.Parameters.AddWithValue("@encryptPassword", encryptPassword);
                                registerInsert.ExecuteNonQuery();//runs query
                                MessageBox.Show("User Registered. Please Log In.", "Success",//msgbox to tell user if successfull
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Asterisk);
                                this.Close();
                                this.RefToForm1.Show();
                            }
                            catch (SqlCeException ex)
                            {
                                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        dbCon.Close();
                    }
                    catch (SqlCeException ex)
                    {
                        MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);//throws error if connection cannot be opened and gives a reason why
                    }
                }
            }
        }
        private void username_Entry_KeyDown_1(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
        }
        private void password_Entry_KeyDown_1(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
        }
        private void confirm_Entry_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
        }
    }
}
