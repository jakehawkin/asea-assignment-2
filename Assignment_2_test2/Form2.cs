﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Assignment_2_test;
using System.Data.SqlServerCe;

namespace Assignment_2
{
    public partial class Form2 : Form
    {
        public Form2(string s)
        {
            InitializeComponent();
            label2.Text = s;
        }
        public Form RefToForm1 //provides a reference to form 1 so it can be opened from the background
        { 
            get; 
            set; 
        }
        private void Form2_FormClosing(object sender, FormClosingEventArgs e)//returns to log in if the form is closed
        {
            this.RefToForm1.Show();
        }

        private void button1_Click(object sender, EventArgs e)//Returns to log in form
        {
            this.RefToForm1.Show();
            this.Close();
        }
    }
}
